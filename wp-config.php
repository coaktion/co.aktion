<?php
/**
 * As configurações básicas do WordPress
 *
 * O script de criação wp-config.php usa esse arquivo durante a instalação.
 * Você não precisa usar o site, você pode copiar este arquivo
 * para "wp-config.php" e preencher os valores.
 *
 * Este arquivo contém as seguintes configurações:
 *
 * * Configurações do MySQL
 * * Chaves secretas
 * * Prefixo do banco de dados
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** Configurações do MySQL - Você pode pegar estas informações com o serviço de hospedagem ** //
/** O nome do banco de dados do WordPress */
define( 'DB_NAME', 'coaktion' );

/** Usuário do banco de dados MySQL */
define( 'DB_USER', 'root' );

/** Senha do banco de dados MySQL */
define( 'DB_PASSWORD', '' );

/** Nome do host do MySQL */
define( 'DB_HOST', 'localhost' );

/** Charset do banco de dados a ser usado na criação das tabelas. */
define( 'DB_CHARSET', 'utf8mb4' );

/** O tipo de Collate do banco de dados. Não altere isso se tiver dúvidas. */
define( 'DB_COLLATE', '' );

/**#@+
 * Chaves únicas de autenticação e salts.
 *
 * Altere cada chave para um frase única!
 * Você pode gerá-las
 * usando o {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org
 * secret-key service}
 * Você pode alterá-las a qualquer momento para invalidar quaisquer
 * cookies existentes. Isto irá forçar todos os
 * usuários a fazerem login novamente.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '=m3upO^73Qyb4:|C`MRLOy@O{Dg+0!Lq2!4|p+F[:CJ ~,#SE1g},Rm>tH|aQiHb' );
define( 'SECURE_AUTH_KEY',  '{-0kv+!qjY?Qv:ipU|?G}60}#!3yVb@(q(z{G7AIrpIS3,7Y)cV%YTiZ]QRJ7yO8' );
define( 'LOGGED_IN_KEY',    'n#yjeR;!Hr(-Xk6JLBo+B0OAPT~mDY5K@KMWC]s*e|O,7c[~Lan2kf{#PWo57z_.' );
define( 'NONCE_KEY',        'RP[pmX`/{sM,Iv7]$zY~AM0~=^}m%tdU<NOMs/A#y,2=YhiN8ZA7y2uV@QWiaH+3' );
define( 'AUTH_SALT',        '*(9H54A]V2pc>#:O>R<vHs Xzk8#`mCm`:*R&O;zZ;z<3BlGO|`:j^;e?%&G+&S:' );
define( 'SECURE_AUTH_SALT', 'gjDyMpjg)zj`wA:[2KFd(U>&Bb.eI+r^o)8Z|Wzu_3 E_4Srz2cZxRGlshl-odbR' );
define( 'LOGGED_IN_SALT',   '$w<sri#&(= @g,VuuW7Z+SMQw6XB3sg08qSwfaItrT&=L4/]SfL&8!5En?UPC&)P' );
define( 'NONCE_SALT',       'VF/rDV!0#|ukL,(.P8g_aN.&Ek*l}m{ 9up$RL7WVw{[_;&xWNkcw3~fbV{j#:v/' );

/**#@-*/

/**
 * Prefixo da tabela do banco de dados do WordPress.
 *
 * Você pode ter várias instalações em um único banco de dados se você der
 * um prefixo único para cada um. Somente números, letras e sublinhados!
 */
$table_prefix = 'wp_';

/**
 * Para desenvolvedores: Modo de debug do WordPress.
 *
 * Altere isto para true para ativar a exibição de avisos
 * durante o desenvolvimento. É altamente recomendável que os
 * desenvolvedores de plugins e temas usem o WP_DEBUG
 * em seus ambientes de desenvolvimento.
 *
 * Para informações sobre outras constantes que podem ser utilizadas
 * para depuração, visite o Codex.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* Isto é tudo, pode parar de editar! :) */

/** Caminho absoluto para o diretório WordPress. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Configura as variáveis e arquivos do WordPress. */
require_once ABSPATH . 'wp-settings.php';
